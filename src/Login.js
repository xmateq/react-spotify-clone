import React from 'react';
import './Login.css';
import { loginUrl } from './spotify';

function Login () {
  return (
    <div className="login">
      <img src="https://appleworld.pl/wp-content/uploads/2020/10/spotify-logo-1920x1080_fouoik.jpg" alt="logo" />
      <a href={loginUrl}>LOGIN WITH SPOTIFY</a>
    </div>
  )
}

export default Login;