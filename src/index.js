import { StrictMode } from "react";
import ReactDOM from "react-dom";
import { DataLayer } from './DataLayer';
import reducer, { initialState } from './reducer';

import App from "./App";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <StrictMode>
    <DataLayer initialState={initialState} reducer={reducer}>
      <App />
    </DataLayer>
  </StrictMode>,
  rootElement
);
